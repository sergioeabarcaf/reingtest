"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "articleController", {
  enumerable: true,
  get: function () {
    return _articleControllers.default;
  }
});

var _articleControllers = _interopRequireDefault(require("./articleControllers"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }