"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _services = require("../services");

// return list of articles record in mongoDB
const list = async (req, res) => {
  const articles = await _services.articleService.list();
  return res.json(articles);
}; // return list of articles in order newest after hide article


const del = async (req, res) => {
  const articles = await _services.articleService.del(req.params);
  return res.json(articles);
};

var _default = {
  list,
  del
};
exports.default = _default;