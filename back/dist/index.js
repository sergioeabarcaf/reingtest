"use strict";

var _axios = _interopRequireDefault(require("axios"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var _server = _interopRequireDefault(require("./server"));

var _articleService = _interopRequireDefault(require("./services/articleService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// imports
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
}); // get articles from hn.algolia.com.


function getNewArticlesFromURL() {
  return _axios.default.get(process.env.URL).then(resp => resp.data).catch(err => err);
} // save articles in mongodb


async function saveArticles(articlesList) {
  const saveStatus = await _articleService.default.create(articlesList);
  return saveStatus;
} // function articles


async function articles() {
  const newArticles = await getNewArticlesFromURL(); // valid exist hits in articles

  if (!newArticles.hits) {
    return false;
  }

  const ready = await saveArticles(newArticles);
  return ready;
} // lister server on PORT and connect mongoDB.


_server.default.listen(process.env.PORT, async err => {
  if (err) throw err;
  return _mongoose.default.connect(process.env.MONGODB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(() => {
    console.log(`MongoDB connected and server listening on port ${process.env.PORT}`);
    articles(); // every 1 hour the articles function is activated.

    setInterval(articles, process.env.TIMEINTERVAL);
  }).catch(errDB => {
    console.error(errDB);
  });
});

console.log(process.env.PORT);