"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "articleService", {
  enumerable: true,
  get: function () {
    return _articleService.default;
  }
});

var _articleService = _interopRequireDefault(require("./articleService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }