"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Article", {
  enumerable: true,
  get: function () {
    return _article.default;
  }
});

var _mongoose = _interopRequireDefault(require("mongoose"));

var _article = _interopRequireDefault(require("./article"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose.default.set('debug', true);