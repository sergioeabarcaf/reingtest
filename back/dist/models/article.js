"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ArticleSchemma = new _mongoose.default.Schema({
  visible: Boolean,
  created_at: String,
  title: String,
  url: String,
  author: String,
  points: Number,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: String,
  story_title: String,
  story_url: String,
  parent_id: String,
  created_at_i: Number,
  _tags: Array,
  objectID: String,
  _highlightResult: {
    title: {
      value: String,
      matchLevel: String,
      matchedWords: Array
    },
    url: {
      value: String,
      matchLevel: String,
      fullyHighlighted: Boolean,
      matchedWords: Array
    },
    author: {
      value: String,
      matchLevel: String,
      matchedWords: Array
    },
    comment_text: {
      value: String,
      matchLevel: String,
      fullyHighlighted: Boolean,
      matchedWords: Array
    }
  }
});

var _default = _mongoose.default.model('Article', ArticleSchemma);

exports.default = _default;