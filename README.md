
# Test Reing Fullstack Developer
This project contains the 'front' and the 'back' of the test sent by REING to apply for the position of fullstack developer. Below is the description of the test.

_The app should have two separate components: the Server and the Client.
The server should take care of pulling the data into the database and expose an API for the Angular client. The client should render a web page that lists the articles in chronological order._

**Server Component**
_Once an hour, the server app should connect to this API which shows recently posted articles about Node.js on Hacker News: http://hn.algolia.com/api/v1/search_by_date?query=nodejs
The server app should insert the data from the API into a MongoDB database and also define a REST API which the client will use to retrieve the data._

**Client Component**
_The user should be able to view a web page which shows the most recent posts in date order (newer first). They should be able to click the delete button to delete an individual post from this view. One a post is deleted it should not reappear, even if the HN API returns it. Also, keep an eye on how the date is presented on each row in the wireframes._

**Stack**
_You must use the following technologies to build the app:_
* _Server component: Node.js + Express/Koa + MongoDB_
* _Client component: latest version of Angular +​ ​Angular Material​ ​or latest versionReact + Material-UI_

## Starting

_the instructions below allow you to install this project and run it on your machine_ 

### pre-requirements
_It is necessary to have the following installed_
```
Docker
Docker-Compose
```

### installation

* first clone this repo [reingtest](https://gitlab.com/sergioeabarcaf/reingtest.git)
```
git clone https://gitlab.com/sergioeabarcaf/reingtest
```
* enter directory 
```
cd reingtest
```
* build docker-compose
```
docker-compose build
```
* up services
```
docker-compose up
```

### use
_for use app, open you browser with URL_
```
localhost
```

## Author
This project was made by Sergio Abarca Flores
* [Linkedin](https://www.linkedin.com/in/sergioeabarcaf/)
* [Github](https://github.com/sergioeabarcaf)